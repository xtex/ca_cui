{
    CLIENT_ID: '31554daed856b5f99a51098cd54a1355e3f2f7cd2cdffd9fdd7dc6d94de3cfc6',
    CLIENT_SECRET: '49728df4a3903e2164643dc775b806987211b63b3b9143f971c75a816eeee872',
    FILE_NAME: 'data/gitee.dat',
    OAUTH_HOST: 'https://gitee.com/oauth',
    GITEE_HOST: 'https://gitee.com/api/v5',
    REDIRECT_URI: 'https://projectxero.gitee.io/ca/feedback.html',
    TARGET_REPO: 'projectxero/ca',
	TARGET_NS: 'projectxero',
	TARGET_RP: 'ca',
    SCOPE: 'user_info issues notes',
    PER_PAGE: 5,

    info: {},
    saveInfo: function (result) {
        fs.writeFileSync(this.FILE_NAME, Safe.base64.encode(JSON.stringify(result)));
    },
    readInfo: function () {
        if (!fs.existsSync(this.FILE_NAME))
            return;
        this.info = JSON.parse(Safe.base64.decode(fs.readFileSync(this.FILE_NAME).toString()));
    },
    logout: function () {
        fs.unlinkSync(this.FILE_NAME);
        this.info = {};
    },
    checklogin: function () {
        if (!this.info.access_token) {
            console.error("Error: no login");
            return false;
        }
        return true;
    },
    loginWeb: function () {
        Utils.openURL(this.OAUTH_HOST + '/authorize?' + Network.toQueryString({
                'client_id': this.CLIENT_ID,
                'redirect_uri': this.REDIRECT_URI,
                'response_type': 'code',
                'scope': this.SCOPE
            }));
        console.log('已在浏览器中打开.');
    },
    codeToToken: function (code) {
        var req = https.request(this.OAUTH_HOST + '/token?' + Network.toQueryString({
                    'grant_type': 'authorization_code',
                    'code': code,
                    'redirect_uri': this.REDIRECT_URI,
                    'client_id': this.CLIENT_ID,
                    'client_secret': this.CLIENT_SECRET
                }), Network.options.feed.post, Network.requestWrap({
                    callback: function (data) {
                        data = JSON.parse(data);
                        if (data.scope != Feedback.SCOPE) {
                            return console.error('请给予权限! 需要权限: ' + Feedback.SCOPE + ', 给予权限: ' + data.scope);
                        }
                        Feedback.saveInfo(data);
                    }
                }));
        req.on('error', console.error);
        req.end();
    },
    showInfo: function () {
        if (!this.checklogin())
            return;
        https.get(this.GITEE_HOST + '/user?' + Network.toQueryString({
                'access_token': this.info.access_token
            }), Network.options.feed.get, Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    var level = data.experience / User.LEVEL_EXPERIENCE;
                    console.log("用户ID: " + data.id);
                    console.log("登录名: " + data.login);
                    console.log("用户名: " + data.name);
                    if (data.blog != '')
                        console.log("博客: " + data.blog);
                    if (data.weibo != '')
                        console.log("微博: " + data.weibo);
                    console.log("公开仓库数: " + data.public_repos);
                    console.log("注册时间: " + new Date(data.created_at).toLocaleString());
                    console.log("描述: " + data.bio);
                }
            })).on('error', console.error);
    },
    showAvatar: function () {
        if (!this.checklogin())
            return;
        https.get(this.GITEE_HOST + '/user?' + Network.toQueryString({
                'access_token': this.info.access_token
            }), Network.options.feed.get, Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    Utils.openURL(data.avatar_url);
                    console.log('已在浏览器中打开');
                }
            })).on('error', console.error);
    },
    starRepo: function (path) {
        if (!this.checklogin())
            return;
        var req = https.request(this.GITEE_HOST + '/user/starred/' + path, Network.options.feed.put, Network.requestWrap({
                    callback: function () {
                        console.log('完成!');
                    }
                }));
        req.write(JSON.stringify({
                'access_token': this.info.access_token
            }));
        req.on('error', console.error);
        req.end();
    },
    unstarRepo: function (path) {
        if (!this.checklogin())
            return;
        var req = https.request(this.GITEE_HOST + '/user/starred/' + path + '?' + Network.toQueryString({
                    'access_token': this.info.access_token
                }), Network.options.feed.del, Network.requestWrap({
                    callback: function () {
                        console.log('完成!');
                    }
                }));
        req.on('error', console.error);
        req.end();
    },
    showIssues: function (page, state) {
        if (!this.checklogin())
            return;
        https.get(this.GITEE_HOST + '/repos/' + this.TARGET_REPO + '/issues?' + Network.toQueryString({
                'state': state,
                'sort': 'created',
                'desc': 'direction',
                'per_page': this.PER_PAGE,
                'page': page
            }), Network.options.feed.get, Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    data.forEach((item) => {
                        console.log(item.title);
                        console.log('ID: ' + item.number);
                        console.log('用户: ' + item.user.name);
                        if (state == 'all')
                            console.log('状态: ' + msgs.feed.issue_state[item.state]);
                        if (item.labels.length != 0) {
                            var labelInfo = '';
                            item.labels.forEach((i, iindex) => {
                                if (iindex != 0)
                                    labelInfo += ', ';
                                labelInfo += i.name;
                            });
                            console.log('标签: ' + labelInfo);
                        }
                        if (item.body != "")
                            console.log('正文: ' + (item.body.length > 16 ? item.body.slice(0, 15) + '...' : item.body));
                        console.log(SPLIT_LINE);
                    });
                }
            })).on('error', console.error);
    },
    showIssueInfo: function (number) {
        if (!this.checklogin())
            return;
        https.get(this.GITEE_HOST + '/repos/' + this.TARGET_REPO + '/issues/' + number.toUpperCase(),
            Network.options.feed.get,
            Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    console.log('ID: ' + data.number);
                    console.log('标题: ' + data.title);
                    console.log('用户: ' + data.user.name);
                    console.log('状态: ' + msgs.feed.issue_state[data.state]);
                    console.log('创建于 ' + new Date(data.created_at).toLocaleString());
                    console.log('共有 ' + data.comments + ' 条评论' + (data.comments > 100 ? ' (警告: CUI只能显示最多100条评论)' : ''));
                    if (data.finished_at != null)
                        console.log('完成于 ' + new Date(data.finished_at).toLocaleString());
                    if (data.labels.length != 0) {
                        var labelInfo = '';
                        data.labels.forEach((i, iindex) => {
                            if (iindex != 0)
                                labelInfo += ', ';
                            labelInfo += i.name;
                        });
                        console.log('标签: ' + labelInfo);
                    }
                    console.log('正文: ' + data.body);
                }
            })).on('error', console.error);
    },
    showIssueComments: function (number) {
        if (!this.checklogin())
            return;
        https.get(this.GITEE_HOST + '/repos/' + this.TARGET_REPO + '/issues/' + number.toUpperCase() + '/comments',
            Network.options.feed.get,
            Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    data.forEach((item) => {
                        console.log('用户: ' + item.user.name);
                        console.log('ID: ' + item.id);
                        console.log('创建于 ' + new Date(item.created_at).toLocaleString());
                        console.log('正文: ' + item.body);
                        console.log(SPLIT_LINE);
                    });
                }
            })).on('error', console.error);
    },
    deleteComment: function (number) {
        if (!this.checklogin())
            return;
        var req = https.request(this.GITEE_HOST + '/repos/' + this.TARGET_REPO + '/issues/comments/' + number + '?' + Network.toQueryString({
                    'access_token': this.info.access_token
                }), Network.options.feed.del, Network.requestWrap({
                    callback: function () {
                        console.log('完成!');
                    }
                }));
        req.on('error', console.error);
        req.end();
    },
    addComment: function (number, body) {
        if (!this.checklogin())
            return;
        var req = https.request(this.GITEE_HOST + '/repos/' + this.TARGET_REPO + '/issues/' + number.toUpperCase() + '/comments',
                Network.options.feed.post, Network.requestWrap({
                    callback: function () {
                        console.log('完成!');
                    }
                }));
        req.write(JSON.stringify({
                'access_token': this.info.access_token,
                'body': body
            }));
        req.on('error', console.error);
        req.end();
    },
    addIssue: function (title, body, labels) {
        if (!this.checklogin())
            return;
        var req = https.request(this.GITEE_HOST + '/repos/' + this.TARGET_NS + '/issues',
                Network.options.feed.post, Network.requestWrap({
                    callback: function (d) {
                        console.log('完成!');
                    }
                }));
        req.write(JSON.stringify({
                'access_token': this.info.access_token,
				'repo': this.TARGET_RP,
				'title': title,
				'labels': labels,
                'body': body
            }));
        req.on('error', console.error);
        req.end();
    }
};
