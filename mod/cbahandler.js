function CBAHandler(arg) {
    if (!global['readline'])
        global['readline'] = require("readline");
    var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    var wait = (msg, callback) => rl.question(msg + '\n回车继续...', callback || (() => {}));
    var exit = () => {
        rl.close();
        process.exit();
    };
    try { // 处理无'//'的URL
        arg = arg.replace('://', ':').replace(':', '://');
        // 解析URL
        var _url = new URL(arg);
        // 检查协议
        if (_url.protocol != 'cbassistant:') {
            wait('未知协议!', exit);
            return;
        }
        // 解析路径
        var childTypes = null;
        if (_url.pathname != null)
            childTypes = _url.pathname.split('/').slice(1);
        var search = _url.searchParams;
        // 处理
        switch (_url.host) {
        case 'base': //基本
            switch (childTypes[0]) {
            case 'open': // 打开
                switch (childTypes[1]) {
                case 'default': // 正常打开
                    wait('网页尝试打开CA', exit);
                    return;
                default:
                    wait('未知操作(base/open): ' + childTypes[1], exit);
                    return;
                }
                break;
            case 'command': // 命令
                switch (childTypes[1]) {
                case 'edit': // 编辑命令
                    if (!search.has('text')) {
                        wait('网页尝试编辑命令但未指定命令', exit);
                        return;
                    }
                    wait('网页尝试编辑命令: ' + decodeURIComponent(search.get('text')), exit);
                    return;
                default:
                    wait('未知操作(base/command): ' + childTypes[1], exit);
                    return;
                }
                break;
            case 'push': // 推送
                switch (childTypes[1]) {
                case 'settings': // 推送设置
                    wait('网页尝试打开推送设置', exit);
                    return;
                default:
                    wait('未知操作(base/push): ' + childTypes[1], exit);
                    return;
                }
                break;
			case 'user': // 用户系统
                switch (childTypes[1]) {
                case 'login': // 登录
                    wait('网页尝试打开用户系统登录窗口', exit);
                    return;
				case 'info': // 信息
                    wait('网页尝试更新用户系统信息', exit);
                    return;
                default:
                    wait('未知操作(base/user): ' + childTypes[1], exit);
                    return;
                }
                break;
            case 'debug': // 调试(仅CUI)
                console.log(_url, childTypes);
                wait('', exit);
                return;
            case 'feedback':
                switch (childTypes[1]) {
                case 'authorize': // 授权回调
                    if (search.has('error')) {
                        wait('Gitee登录错误: ' + search.get('error'), exit);
                    } else {
                        console.log('回调Code: ' + search.get('code') + ' 状态: ' + search.get('state'));
                        console.log('请稍后, 正在处理...');
                        Feedback.codeToToken(search.get('code'));
                        rl.close();
                    }
                    return;
                default:
                    wait('未知操作(base/feedback): ' + childTypes[1], exit);
                    return;
                }
                break;
            default:
                wait('未知操作(base): ' + childTypes[0], exit);
                return;
            }
            break;
        default:
            wait('未知操作: ' + _url.host, exit);
            return;
        }
        // 退出
        exit();
    } catch (err) {
        wait('CBAssistant处理错误: ' + err, exit);
    }
};
