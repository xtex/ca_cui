{
    toQueryString: function (obj) {
        // Copied from CA
        var i,
        r = [];
        for (i in obj) {
            if (obj[i] == undefined)
                continue;
            r.push(i + "=" + encodeURIComponent(obj[i]));
        }
        return r.join("&");
    },
    requestWrap: function (o) {
        var f = function NetworkRequestWrapper(res) {
            console.debug('状态码:', res.statusCode);
            console.debug('请求头:', res.headers);
            var data = "";

            res.on('data', (d) => {
                data += d;
            });
            res.on('end', () => {
                NetworkRequestWrapper.callback(data);
            });
        }
        f.callback = o.callback;
        return f;
    },
	initial: function() {
		this.options.user.post.headers = this.options.user.get.headers;
		this.options.feed.post.headers = this.options.feed.get.headers;
		this.options.feed.put.headers = this.options.feed.get.headers;
		this.options.feed.del.headers = this.options.feed.get.headers;
	},
    options: {
        user: {
            get: {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            },
            post: {
                method: 'post'
            }
        },
        feed: {
            get: {
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
					'Content-Type': 'application/json;charset=UTF-8'
                }
            },
            post: {
                method: 'post'
            },
            put: {
                method: 'put'
            },
            del: {
                method: 'delete'
            }
        }
    }
};
