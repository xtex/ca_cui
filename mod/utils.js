{
    openURL: function (url) { // Source: https://blog.csdn.net/qq1036548849/article/details/86470140
        var exec = require('child_process').exec;
        switch (process.platform) {
        case "darwin":
            exec('open ' + url);
            break;
        case "win32":
            exec('start explorer \"' + url + '\"');
            break;
        default:
            exec('xdg-open', [url]);
        }
    },
    line: function (msg, callback) {
        const readline = require("readline");
        const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
        rl.question(msg, (answer) => {
            rl.close();
            callback(answer);
        });
    }
};
