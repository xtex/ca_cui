{
    LEVEL_EXPERIENCE: 25,
    NO_REFRESH_TASKS: ['login', 'register', 'forgot'],
    STATUS: msgs.user.status,
    FAST_EXP_REASONS: msgs.user.fastExpReasons,
    FILE_NAME: 'data/login.dat',

    info: {},
    login: function (username, pwd) {
        var req = https.request(APIHOST + '/user/login', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        data = JSON.parse(data);
                        if (data.code != 0) {
                            console.error("Error: " + msgs.user[data.error]);
                        } else {
                            User.saveInfo(data.result);
                            console.log("Login successful.");
                        }
                    }
                }));
        req.write(Network.toQueryString({
                "name": username,
                "pass": pwd
            }));
        req.on('error', console.error);
        req.end();
    },
    saveInfo: function (result) {
        fs.writeFileSync(this.FILE_NAME, Safe.base64.encode(JSON.stringify(result)));
    },
    readInfo: function () {
        if (!fs.existsSync(this.FILE_NAME))
            return;
        this.info = JSON.parse(Safe.base64.decode(fs.readFileSync(this.FILE_NAME).toString()));
    },
    showInfo: function () {
        if (!this.checklogin())
            return;
        https.get(APIHOST + '/user/info?' + Network.toQueryString({
                token: this.info.accessToken
            }), Network.options.user.get, Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    if (data.code != 0) {
                        console.error("Error: " + msgs.user[data.error]);
                    } else {
                        data = data.result;
                        var level = data.experience / User.LEVEL_EXPERIENCE;
                        console.log("用户ID: " + data.id);
                        console.log("邮箱: " + data.email);
                        console.log("用户名: " + data.name);
                        console.log("状态: " + data.status + (User.STATUS[data.status] ? '(' + User.STATUS[data.status] + ')' : ''));
                        console.log("注册时间: " + new Date(data.register_time).toLocaleString());
                        console.log("经验: " + data.experience);
                        console.log("上次经验: " + data.yesterday_experience);
                        console.log("签到: " + (data.checkedIn ? "已签到" : "未签到"));
                        console.log("Lv " + level.toFixed(0) + "(" + level + ")");
                    }
                }
            })).on('error', console.error);
    },
    logout: function () {
        fs.unlinkSync(this.FILE_NAME);
        this.info = {};
    },
    checklogin: function () {
        if (!this.info.accessToken) {
            console.error("Error: no login");
            return false;
        }
        return true;
    },
    register: function (username, email, pwd) {
        var req = https.request(APIHOST + '/user/register', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("注册完成, 请检查邮箱.");
                    }
                }));
        req.write(Network.toQueryString({
                "name": username,
                "email": email,
                "pass": pwd
            }));
        req.on('error', console.error);
        req.end();
    },
    forgotPwd: function (email, pwd) {
        var req = https.request(APIHOST + '/user/forget_password', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("完成, 请检查邮箱.");
                    }
                }));
        req.write(Network.toQueryString({
                "email": email,
                "pass": pwd
            }));
        req.on('error', console.error);
        req.end();
    },
    setPwd: function (oldpwd, newpwd) {
        if (!this.checklogin())
            return;
        var req = https.request(APIHOST + '/user/set_password', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("设置完成");
                    }
                }));
        req.write(Network.toQueryString({
                "accessToken": this.info.accessToken,
                "oldPwd": oldpwd,
                "newPwd": newpwd
            }));
        req.on('error', console.error);
        req.end();
    },
    setName: function (username) {
        if (!this.checklogin())
            return;
        var req = https.request(APIHOST + '/user/set_username', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("设置完成");
                    }
                }));
        req.write(Network.toQueryString({
                "accessToken": this.info.accessToken,
                "name": username
            }));
        req.on('error', console.error);
        req.end();
    },
    changeEmail: function (email) {
        if (!this.checklogin())
            return;
        var req = https.request(APIHOST + '/user/change_email', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("完成, 请检查邮箱.");
                    }
                }));
        req.write(Network.toQueryString({
                "accessToken": this.info.accessToken,
                "email": email
            }));
        req.on('error', console.error);
        req.end();
    },
    checkin: function () {
        if (!this.checklogin())
            return;
        var req = https.request(APIHOST + '/user/check_in', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        console.log("签到完成");
                    }
                }));
        req.write(Network.toQueryString({
                "token": this.info.accessToken
            }));
        req.on('error', console.error);
        req.end();
    },
    addexp: function (reasons) {
        if (!this.checklogin())
            return;
        var req = https.request(APIHOST + '/user/add_exp', Network.options.user.post, Network.requestWrap({
                    callback: function (data) {
                        data = JSON.parse(data);
                        if (data.code != 0) {
                            console.error("Error: " + msgs.user[data.error]);
                        } else {
                            data = data.result;
                            console.log("经验: " + data.experience);
                            console.log("本次增加: " + data.add);
                            console.log("今日增加: " + data.today_experience);
                            console.log("今日最多: " + data.max_today_exp);
                        }
                    }
                }));
        req.write(Network.toQueryString({
                "token": this.info.accessToken,
                "reasons": reasons.join(',')
            }));
        req.on('error', console.error);
        req.end();
    },
    fastAddExp: function () {
        if (!this.checklogin())
            return;
        this.checkin();
        var reasons = [];
        for (n in this.FAST_EXP_REASONS) {
            for (var i = 0; i < this.FAST_EXP_REASONS[n]; i++) {
                reasons.push(n);
            }
        }
        this.addexp(reasons);
    },
    refreshToken: function (cb) {
        https.get(APIHOST + '/user/refresh?' + Network.toQueryString({
                token: User.info.refreshToken
            }), Network.requestWrap({
                callback: function (data) {
                    data = JSON.parse(data);
                    if (data.code != 0) {
                        console.error("RefreshError: " + msgs.user[data.error]);
                    } else {
                        User.saveInfo(data.result);
                        User.info = data.result;
                    }
                    cb();
                }
            }));
    }
};
