function Analy() {
    https.get(apihost + '/analytics', Network.requestWrap({
            callback: function (data) {
                data = JSON.parse(data);
                if (data.status != 200) {
                    console.error("Error: " + data.status + ": " + data.message);
                } else {
                    data.result.forEach((item) => {
                        console.log("时间:           " + item.daily);
                        console.log("本周活跃用户量: " + item.wau);
                        console.log("本月活动用户量: " + item.mau);
                        console.log("累计用户量:     " + item.totaluser);
                        console.log("新用户量:       " + item.newuser);
                        console.log("活动用户量:     " + item.activeuser);
                        console.debug("会话: " + item.session);
                        console.log(SPLIT_LINE);
                    });
                }
            }
        })).on('error', console.error);
};
