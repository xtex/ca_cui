module.exports = {
    'user': {
        'status': {
            0: '正常',
            999: '管理员'
        },
        'fastExpReasons': {
            'createTemplate': 6,
            'copyCommand': 2
        },

        'precond.user.info.token.missing': '缺少访问令牌',
        'error.user.info.invalidToken': '无效的访问令牌',
        'error.user.info.userNotExist': '用户已被注销',

        'precond.user.register.email.missing': '缺少邮箱地址',
        'precond.user.register.email.wrong': '邮箱地址过长或格式不正确',
        'precond.user.register.name.missing': '缺少用户名',
        'precond.user.register.name.wrong': '用户名长度过短、过长或含有违法词语',
        'precond.user.register.pass.missing': '缺少密码',
        'precond.user.register.pass.wrong': '密码过短或过长',
        'error.user.register.emailOccupied': '邮箱已被使用',
        'error.user.register.nameOccupied': '用户名已被占',
        'error.user.register.abuseEmail': '请不要向这个邮箱发送过多邮件或发送邮件发送过于频繁',
        'error.user.register.writeError': '写入数据库失败',
        'error.user.register.sendEmailFailed': '发送验证邮件失败',

        /*'precond.user.activate.token.missing' : '缺少token参数',
        'error.user.activate.invalidToken' : '链接已被使用过或参数不正确',
        'error.user.activate.emailOccupied' : '邮箱已被占用',
        'error.user.activate.nameOccupied' : '用户名已被占用',
        'error.user.activate.writeError' : '写入账户记录失败',*/

        'precond.user.login.user.missing': '缺少用户名',
        'precond.user.login.pass.missing': '缺少密码',
        'error.user.login.userNotExist': '用户名或密码不正确',
        'error.user.login.wrongPassword': '用户名或密码不正确',
        'error.user.login.writeError': '写入数据库失败',

        'precond.user.refreshLogin.token.missing': '缺少刷新令牌',
        'error.user.refreshLogin.writeError': '写入数据库失败',
        'error.user.refreshLogin.invalidToken': '刷新令牌不可用',
        'error.user.refreshLogin.writeError': '写入数据库失败',

        'precond.user.authorize.token.missing': '缺少刷新令牌',
        'error.user.authorize.writeError': '写入数据库失败',
        'error.user.authorize.invalidToken': '刷新令牌不可用',

        'precond.user.loginOAuth.token.missing': '授权令牌不可用',
        'error.user.loginOAuth.invalidToken': '已取得OAuth令牌或授权令牌不可用',

        'precond.user.forgetPwd.email.missing': '缺少邮箱地址',
        'precond.user.forgetPwd.pass.missing': '缺少密码',
        'precond.user.forgetPwd.pass.wrong': '密码过短或过长',
        'error.user.forgetPwd.userNotExist': '没有账户和该邮箱绑定',
        'error.user.forgetPwd.abuseEmail': '请不要向这个邮箱发送过多邮件或发送邮件发送过于频繁',
        'error.user.forgetPwd.sendEmailFailed': '发送重置密码邮件失败',

        /*'precond.user.resetPwd.token.missing' : '缺少token参数',
        'error.user.resetPwd.invalidToken' : '链接已被使用过或参数不正确',
        'error.user.resetPwd.userNotExist' : '邮箱对应的账户已被注销',
        'error.user.resetPwd.writeError' : '写入账户记录失败',*/

        'precond.user.setPwd.accessToken.missing': '缺少访问令牌',
        'precond.user.setPwd.oldPassword.missing': '缺少旧密码',
        'precond.user.setPwd.newPassword.missing': '缺少新密码',
        'precond.user.setPwd.newPassword.wrong': '密码过短或过长',
        'error.user.setPwd.wrongOldPassword': '旧密码错误',
        'error.user.setPwd.writeError': '写入账户记录失败',

        'precond.user.setName.accessToken.missing': '缺少访问令牌',
        'precond.user.setName.name.missing': '缺少用户名',
        'precond.user.setName.name.wrong': '用户名长度过短、过长或含有违法词语',
        'error.user.setName.nameOccupied': '用户名被占用',
        'error.user.setName.writeError': '写入账户记录失败',

        'precond.user.changeEmail.email.missing': '缺少邮箱地址',
        'precond.user.changeEmail.email.wrong': '邮箱地址过长或格式不正确',
        'info.user.changeEmail.sameAddress': '旧邮箱与新邮箱相同',
        'error.user.changeEmail.emailOccupied': '该邮箱已与其他账户绑定',
        'error.user.changeEmail.sendEmailFailed': '发送验证邮件失败',

        /*'precond.user.confirmEmail.token.missing': '缺少token参数',
        'error.user.confirmEmail.invalidToken': '链接已被使用过或参数不正确',
        'error.user.confirmEmail.userNotExist': '邮箱对应的账户已被注销',
        'error.user.confirmEmail.emailOccupied': '邮箱已与其他账户绑定',
        'error.user.confirmEmail.writeError': '写入账户记录失败',*/

        'precond.user.addExp.token.missing': '缺少访问令牌',
        'precond.user.addExp.reason.missing': '缺少经验来源',
        'precond.user.addExp.reason.wrong': '不是有效的经验来源',
        'error.user.addExp.notAllowed': '在指定时间前，经验已到达上限',
        'info.user.addExp.limitedToday': '本日获得经验已到达上限',
        'error.user.addExp.writeError': '写入账户记录失败',

        'precond.user.checkIn.token.missing': '缺少访问令牌',
        'error.user.checkIn.writeError': '写入账户记录失败',

        'precond.admin.auth.token.missing': '缺少令牌',
        'error.admin.auth.notAdmin': '没有权限',
        'error.admin.auth.invalidToken': '无效的管理员令牌',
        'error.admin.action.notExists': '管理员任务不存在',
        'error.admin.action.error': '管理员任务执行出错'
    },
	'feed': {
		'issue_state': {
			'open': '开启的',
			'closed': '已关闭',
			'rejected': '已拒绝',
			'progressing': '进行中'
		}
	}
}
