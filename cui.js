/* CA-CUI
 * GPLv3
 */
const APIHOST = "https://ca.projectxero.top";
const DEBUG = false;
const msgs = require("./messages.js");
const MODS = ['User', 'Analy', 'CBAHandler', 'Feedback', 'Safe', 'Network', 'Utils'];
const REQS = ['https', 'http', 'fs', 'url'];

const SPLIT_LINE = "--------------------------------------------------------------------------------"; // Length: 80

var argv = process.argv.slice(2);
// Hidden debug output
if (DEBUG == false)
    console.debug = () => {};
// Load modules
REQS.forEach(function (n) {
    global[n] = require(n);
});
MODS.forEach(function (n) {
    eval('var ' + n + ' = ' + fs.readFileSync('./mod/' + n.toLowerCase() + '.js').toString());
    global[n] = eval(n);
    if (global[n].initial)
        global[n].initial();
});

if (argv.length == 0) {
    console.log("CA char user interface  by @xtex");
    return;
}

switch (argv[0]) {
case 'analy': // 统计信息
    Analy();
    break;
case 'user': // 用户模块
    var doUserTask = function () {
        switch (argv[1]) {
        case 'login': // 登录
            User.login(argv[2], argv[3]);
            break;
        case 'info': // 查看信息
            User.showInfo();
            break;
        case 'refresh': // 刷新Token
            break; // Auto refresh
        case 'logout': // 退出登录
            User.logout();
            break;
        case 'register': // 注册
            User.register(argv[2], argv[3], argv[4]);
            break;
        case 'forgot': // 忘记密码
            User.forgotPwd(argv[2], argv[3]);
            break;
        case 'setPwd': // 设置密码
            User.setPwd(argv[2], argv[3]);
            break;
        case 'setName': // 设置用户名
            User.setName(argv[2]);
            break;
        case 'changeEmail': // 更改邮箱
            User.changeEmail(argv[2]);
            break;
        case 'checkin': // 签到
            User.checkin();
            break;
        case 'addexp': // 加经验
            User.addexp(argv.slice(2));
            break;
        case 'fastexp': // 快速加经验
            User.fastAddExp();
            break;
        default:
            console.error('Unknown: ' + argv[1]);
        }
    }
    try {
        User.readInfo();
        if (User.NO_REFRESH_TASKS.indexOf(argv[1])) {
            User.refreshToken(doUserTask);
        } else {
            doUserTask();
        }
    } catch (err) {
        console.error(err);
        doUserTask();
    }
    break;
case 'cba': // CBAssistant
    switch (argv[1]) {
    case 'installer': // 打开安装工具
        require('./tools/cbassistant.js');
        break;
    case 'cb': // 伪协议回调
        CBAHandler(argv[2]);
        break;
    default:
        console.error('Unknown: ' + argv[1]);
    }
    break;
case 'feed': // 反馈
    var doFeedbackTask = function () {
        switch (argv[1]) {
        case 'loginWeb': // 在浏览器登录
            Feedback.loginWeb();
            break;
        case 'info': // 查看用户信息
            Feedback.showInfo();
            break;
        case 'avatar': // 查看用户头像
            Feedback.showAvatar();
            break;
        case 'starca': // Star CA
            Feedback.starRepo(Feedback.TARGET_REPO);
            break;
        case 'unstarca': // Unstar CA
            Feedback.unstarRepo(Feedback.TARGET_REPO);
            break;
        case 'issues': // 查看Issues
            Feedback.showIssues(argv[2] || '1', 'open');
            break;
            showIssueInfo
        case 'issues-all': // 查看全部Issues
            Feedback.showIssues(argv[2] || '1', 'all');
            break;
        case 'issue': // 查看Issue信息
            Feedback.showIssueInfo(argv[2]);
            break;
        case 'comments': // 查看Issue评论
            Feedback.showIssueComments(argv[2]);
            break;
        case 'cmt': // 评论操作
            switch (argv[2]) {
            case 'add': // 创建
                Utils.line(':', (data) => {
                    Feedback.addComment(argv[3], data);
                });
                break;
            case 'del': // 删除
                Feedback.deleteComment(argv[3]);
                break;
            default:
                console.error('Unknown: ' + argv[2]);
            }
            break;
        case 'iss': // Issue操作
            switch (argv[2]) {
            case 'add': // 创建
                Utils.line('请输入标题:', (title) => {
                    Utils.line('请输入正文:', (body) => {
                        Utils.line('请输入标签(如\'feature\'):', (labels) => {
                            Feedback.addIssue(title, body, labels);
                        });
                    });
                });
                break;
            default:
                console.error('Unknown: ' + argv[2]);
            }
            break;
        default:
            console.error('Unknown: ' + argv[1]);
        }
    };
    try {
        Feedback.readInfo();
        doFeedbackTask();
    } catch (err) {
        console.error(err);
        doFeedbackTask();
    }
    break;
case 'debug':
    switch (argv[1]) {
    case 'decJSON':
        console.log(JSON.parse(Safe.base64.decode(fs.readFileSync(argv[2]).toString())));
        break;
    default:
        console.error('Unknown: ' + argv[1]);
    }
    break;
default:
    console.error('Unknown: ' + argv[0]);
}
