@echo off
title CUIConsole
cls
echo CA char user interface console v1.0

:main
set /p CUIC_I=%%%$ 

if "%CUIC_I%"==exit exit

cd ..
node cui %CUIC_I%
cd tools
echo.

goto main
